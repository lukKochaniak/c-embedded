#include "game.h"

Game* newGame(tU16 m_width, tU16 m_height, tU16 m_numOfBlocks){
    Game *game = (Game*)malloc(sizeof(struct m_game));
    game->score = 0;
    game->width = m_width;
    game->height = m_height;
    game->numOfBlocks = m_numOfBlocks;
    game->edges[right] = RIGHT_EDGE;
    game->edges[left] = LEFT_EDGE;
    game->edges[bottom] = BOTTOM_EDGE;
    game->edges[top] = UPPER_EDGE;
    game->distanceFromEdge[right] = DISTANCEFROMTHERIGHTEDGE;
    game->distanceFromEdge[left] = DISTANCEFROMTHELEFTEDGE;
    game->distanceFromEdge[bottom] = DISTANCEFROMTHEBOTTOMEDGE;
    game->distanceFromEdge[top] = DISTANCEFROMTHEUPEDGE;
    game->addPointToGameScore = addPointToGameScore;
    game->getScore = getGameScore;
    game->getWidth = getGameWidth;
    game->getHeight = getGameHeight;
    game->getNumOfBlocks = getGameNumOfBlocks;
    game->getEdge = getGameEdge;
    game->getDistanceFromEdge = getGameDistanceFromEdge;
    Point* lowPoint = newPoint(game->width/2, game->height - (game->distanceFromEdge[top]));
	game->racketPtr = racketInit(game, *lowPoint, LENGTH_OF_RACKET_X, LENGTH_OF_RACKET_Y, PURPLE);
    game->ballPtr = ballInit(game, *lowPoint, LENGTH_OF_RACKET_Y, BALL_RADIUS, RED);
    //blocksInit(game, m_numOfBlocks, LENGTH_OF_BLOCK_X, LENGTH_OF_BLOCK_Y, game->blockPtr);
    deletePoint(lowPoint);
    return game;
}
void deleteGame(Game* game){
    tU16 i = 0;
    deleteBall(game->ballPtr);
    deleteRacket(game->racketPtr);
    for(i = 0; i < game->numOfBlocks; i++)
        deleteBlock(game->blockPtr[i]);
    free(game);
}
void addPointToGameScore(Game* game){
    game->score++;
}
tU16 getGameScore(Game* game){
    return game->score;
}
tU16 getGameWidth(Game* game){
    return game->width;
}
tU16 getGameHeight(Game* game){
    return game->height;
}
tU16 getGameNumOfBlocks(Game* game){
    return game->numOfBlocks;
}
tU16 getGameEdge(Game* game, typeEdges edge){
    return game->edges[edge];
}
tU16 getGameDistanceFromEdge(Game* game, typeEdges edge){
    return game->distanceFromEdge[edge];
}
Ball* ballInit(Game* game, Point m_lowCenter, tU16 m_height, tU16 m_radius, tU16 m_color){
    Point* center = newPoint(m_lowCenter.x, m_lowCenter.y - (m_height + m_height + m_radius));
    Direction* customDirection = newDirection((rand() % 2 ? -1 : 1), -1);
    srand(time(NULL));
    Ball* ball = newBall(*center, m_radius, m_color, *customDirection, game);
    deleteDirection(customDirection);
    deletePoint(center);
    return ball;
}
void blocksInit(Game* game, tU16 numOfBlocks, tU16 blockWidth, tU16 blockHeight, Block** blocks){
    int i;
    Point* m_upperRight = newPoint((game->distanceFromEdge[left] + blockWidth), game->distanceFromEdge[bottom] + blockHeight);
    Point* m_lowerLeft = newPoint((game->distanceFromEdge[left]), game->distanceFromEdge[bottom]);
    tU16 numOfCols = 3;
    i = 0;
    for(i = 0; i < numOfBlocks; i++){
        blocks[i] = newBlock(*m_upperRight, *m_lowerLeft, PURPLE, game);
        m_upperRight->set_x(m_upperRight, m_upperRight->get_x(m_upperRight) + DISTANCE_BETWEEN_BLOCKS_X + blockWidth);
        m_lowerLeft->set_x(m_lowerLeft, m_lowerLeft->get_x(m_lowerLeft) + DISTANCE_BETWEEN_BLOCKS_X + blockWidth);
        if(i % numOfCols == 0)
        {
            m_upperRight->set_x(m_upperRight, game->distanceFromEdge[left] + blockWidth);
            m_lowerLeft->set_x(m_lowerLeft, game->distanceFromEdge[left]);
            m_upperRight->set_y(m_upperRight, m_upperRight->get_y(m_upperRight) + blockHeight + DISTANCE_BETWEEN_BLOCKS_Y);
            m_lowerLeft->set_y(m_lowerLeft, m_lowerLeft->get_y(m_lowerLeft) + blockHeight + DISTANCE_BETWEEN_BLOCKS_Y);
        }
    }

}

Racket* racketInit(Game* game, Point m_lowCenter, tU16 m_width, tU16 m_height, tU16 m_color){
    Point* m_upperRight = newPoint(m_lowCenter.x + m_width/2, HEIGHT - DISTANCEFROMTHEUPEDGE);
    Point* m_lowerLeft = newPoint(m_lowCenter.x - m_width/2, HEIGHT - (DISTANCEFROMTHEUPEDGE + m_height));
    Racket* racket = newRacket(*m_upperRight, *m_lowerLeft, m_color, game);
    deletePoint(m_upperRight);
    deletePoint(m_lowerLeft);
    return racket;
}
