/******************************************************************************
 *
 * Copyright:
 *    (C) 2008 Embedded Artists AB
 *
 * File:
 *    main.c
 *
 * Description:
 *    Sample application that demonstrates how to use the QVGA LCD
 *
 *****************************************************************************/

/******************************************************************************
 * Includes
 *****************************************************************************/
#include "general.h"
#include <printf_P.h>
#include <ea_init.h>
#include <stdlib.h>
#include <consol.h>
#include <lpc24xx.h>
//#include <stdio.h>
#include "ea_lcd/lcd_hw.h"
#include "ea_lcd/lcd_grph.h"
#include "delay.h"
#include "touch.h"
#include "sdram.h"
#include "game.h"
#include "i2c.h"
#include "pca9532.h"

/******************************************************************************
 * Defines and typedefs
 *****************************************************************************/

/*
 * Enabled this define if you would like to test different values
 * for the LCD parameters. You will be able to input the values
 * from a console.
 */
//#define TEST_LCDPARAMS

/*****************************************************************************
 * Local variables
 ****************************************************************************/

/* EA QVGA portrait mode TFT display parameters */
static tLcdParams ea_QVGA_v2 =
{
    28,        /* Horizontal back porch */
    10,       /* Horizontal front porch */
    2,       /* HSYNC pulse width */
    240,      /* Pixels per line */
    3,        /* Vertical back porch */
    2,       /* Vertical front porch */
    2,        /* VSYNC pulse width */
    320,      /* Lines per panel */
    0,        /* Do not invert output enable */
    1,        /* Invert panel clock */
    1,        /* Invert HSYNC */
    1,        /* Invert VSYNC */
    1,        /* AC bias frequency (not used) */
    16,       /* Bits per pixel */
    LCD_ADTFT,    /* LCD panel type */
    0,        /* Single panel display */
};

lcd_color_t const COLORS_TAB[16] = {BLACK,
                             NAVY,
                             DARK_GREEN,
                             DARK_CYAN,
                             MAROON,
                             PURPLE,
                             OLIVE,
                             LIGHT_GRAY,
                             DARK_GRAY,
                             BLUE,
                             GREEN,
                             CYAN,
                             RED,
                             MAGENTA,
                             YELLOW,
                             WHITE
                            };

lcd_color_t const BLOCK_COLORS[14] = {
                             NAVY,
                             DARK_GREEN,
                             DARK_CYAN,
                             MAROON,
                             PURPLE,
                             OLIVE,
                             LIGHT_GRAY,
                             DARK_GRAY,
                             BLUE,
                             GREEN,
                             CYAN,
                             RED,
                             MAGENTA,
                             YELLOW,
                            };
//for sound
extern char wavSound[];

tU32 wavSoundSize();

//for leds
static void delayMs(tU16 delayInMs)
{
  /*
   * setup timer #1 for delay
   */
  T1TCR = 0x02;          //stop and reset timer
  T1PR  = 0x00;          //set prescaler to zero
  T1MR0 = delayInMs * (Fpclk / 1000);
  T1IR  = 0xff;          //reset all interrrupt flags
  T1MCR = 0x04;          //stop timer on match
  T1TCR = 0x01;          //start timer

  //wait until delay time has elapsed
  while (T1TCR & 0x01)
    ;
}

static void udelay( unsigned int delayInUs )
{
  /*
   * setup timer #1 for delay
   */
  T1TCR = 0x02;          //stop and reset timer
  T1PR  = 0x00;          //set prescaler to zero
  T1MR0 = (((long)delayInUs-1) * (long)Fpclk/1000) / 1000;
  T1IR  = 0xff;          //reset all interrrupt flags
  T1MCR = 0x04;          //stop timer on match
  T1TCR = 0x01;          //start timer

  //wait until delay time has elapsed
  while (T1TCR & 0x01)
    ;
}

short* pData = NULL;
tU32 size = 0;
tU32 cnt = 0;
tU32 samples = 0;
tU32 numSamples;
tU32 sampleRate;
tU16 usDelay;
tU16 audioFlag = 1;
short soundCounter = 0;
void audioPoint(){
	DACR = 0x7fc0;
	numSamples = 1200;
	cnt++;
	samples = 0;
	usDelay = 1000000/ 24000 + 1;
	samples = 0;
	cnt = 11 + pData[8]/2;

	while(samples++ < numSamples)
	{
	  tS32 val;

	  val = pData[cnt++];
	  DACR = ((val + 0x7fff) & 0xffc0); // |  //actual value to output

	  udelay(usDelay);
	}
}


void playCollisionSnd(){
  	audioPoint();
}

//for leds
void setLed(int led, int on)
{
  tU8 commandString[] = {0x08, 0x00};
  tU8 reg;

  //adjudt address if LED >= 5
  if (led >= 5)
    commandString[0] = 0x09;

    //read current register value
  pca9532(commandString, 1, &reg, 1);


  //update for new register value
  switch (led)
  {
  case 1:
  case 5:
    reg &= ~0x03;
    if (on == 1)
      reg |= 0x01;
    break;
  case 2:
  case 6:
    reg &= ~0x0c;
    if (on == 1)
      reg |= 0x04;
    break;
  case 3:
  case 7:
    reg &= ~0x30;
    if (on == 1)
      reg |= 0x10;
    break;
  case 4:
  case 8:
    reg &= ~0xc0;
    if (on == 1)
      reg |= 0x40;
    break;
  default:
    break;
  }
  commandString[1] = reg;
  pca9532(commandString, sizeof(commandString), NULL, 0);
}

/*****************************************************************************
 *
 * Description:
 *    Functions representing logic of the game
 *
 ****************************************************************************/
void ledScores(tU16 score){
	switch(score)
	{
		case 1:
			setLed(1, 1);
			break;
		case 2:
			setLed(2, 1);
			break;
		case 3:
			setLed(3, 1);
    		break;
		case 4:
			setLed(4, 1);
			break;
		case 5:
			setLed(5, 1);
    		break;
		case 6:
			setLed(6, 1);
    		break;
		case 7:
			setLed(7, 1);
			break;
		case 8:
			setLed(8, 1);
			break;
	}

}

void drawBlock(tU16 currentUpperLeftY, tU16 currentUpperLeftX, tU16 currentLowerRightX, tU16 currentLowerRightY, tU16 flag){
	if(flag == 0)
		lcd_fillRect(currentUpperLeftX, currentUpperLeftY, currentLowerRightX, currentLowerRightY, BLOCK_COLORS[(RTC_HOUR + RTC_MIN + RTC_SEC)%14]);
	else
		lcd_fillRect(currentUpperLeftX, currentUpperLeftY, currentLowerRightX, currentLowerRightY, BLACK);

	//		lcd_fillRect(currentUpperLeftX, currentUpperLeftY, currentLowerRightX, currentLowerRightY, WHITE);

}

tU16 isIntersectingBlockBall(tU16 currentUpperLeftY, tU16 currentUpperLeftX, tU16 currentLowerRightX, tU16 currentLowerRightY, tU16 flag, tU16 currentCenterX, tU16 currentCenterY, tU16 radius){
	if(flag == 1)
        return 0;
    return ((currentLowerRightX  >= (currentCenterX - radius))
        && (currentUpperLeftX    <= (currentCenterX + radius))
        && (currentUpperLeftY    >= (currentCenterY - radius))
        && (currentLowerRightY   <= (currentCenterY + radius)));
}

tU16 testCollisionBlockBall(tU16 currentUpperLeftY, tU16 currentUpperLeftX, tU16 currentLowerRightX, tU16 currentLowerRightY, tU16 flag, tU16 currentCenterX, tU16 currentCenterY, tU16 radius, tS16 *direction_x, tS16 *direction_y){
    if(!isIntersectingBlockBall(currentUpperLeftY, currentUpperLeftX, currentLowerRightX, currentLowerRightY, flag, currentCenterX, currentCenterY, radius))
        return 0;

//    *direction_x *= -1;
//    *direction_y *= -1;
//    if(currentLowerRightX  >= (currentCenterX - radius)){
//        *direction_x *= -1;
//    }
//    else if(currentUpperLeftX    <= (currentCenterX + radius)){
//    	*direction_x  *= -1;
//    }
//
    if(currentUpperLeftY    >= (currentCenterY - radius)){
    	*direction_y *= -1;
    }
    else if(currentLowerRightY   <= (currentCenterY + radius)){
    	*direction_y *= -1;
    }
    return 1;
}

void testCollisionEdgeBall(tU16 currentCenterX, tU16 currentCenterY, tU16 radius, tS16 *direction_x, tS16 *direction_y, tU16 *gameOver){
    /*if(!isIntersectingEdgeBall(game, game->ballPtr))
        return;*/
    if((currentCenterX - radius) <= LEFT_EDGE){
        *direction_x *= -1;
    	playCollisionSnd();
    }
    if((currentCenterX + radius) >= RIGHT_EDGE){
        *direction_x *= -1;
    	playCollisionSnd();
    }
    if((currentCenterY + radius) >= UPPER_EDGE){
        *direction_y *= -1;
    	playCollisionSnd();
    }
    if((currentCenterY - radius) <= BOTTOM_EDGE){
        *gameOver = 1;
    	playCollisionSnd();
    }
    return;
}

void drawRacket(tU16 currentUpperLeftY, tU16 currentUpperLeftX, tU16 currentLowerRightX, tU16 currentLowerRightY){
	lcd_fillRect(currentUpperLeftX, currentUpperLeftY, currentLowerRightX, currentLowerRightY, YELLOW);
}

void drawBall(tU16 currentCenterX, tU16 currentCenterY, tU16 radius){
	lcd_fillcircle(currentCenterX, currentCenterY, radius, YELLOW);
}

void moveBall(tU16 *currentCenterX, tS16 direction_x, tU16 *currentCenterY, tS16 direction_y, tU16 *radius){
	*currentCenterX += direction_x;
	*currentCenterY += direction_y;
}

void eraseRacket(tU16 currentUpperLeftY, tU16 currentUpperLeftX, tU16 currentLowerRightX, tU16 currentLowerRightY)
{
	lcd_fillRect(currentUpperLeftX, currentUpperLeftY, currentLowerRightX, currentLowerRightY, BLACK);
}

void eraseBall(tU16 currentCenterX, tU16 currentCenterY, tU16 radius)
{
	lcd_fillcircle(currentCenterX, currentCenterY, radius, BLACK);
}

#define FALSE 0
#define TRUE 1

tU16 joystickMovement(tU16 *currentRacketUpperLeftY, tU16 *currentRacketUpperLeftX, tU16 *currentRacketLowerRightX, tU16 *currentRacketLowerRightY){
	if ((FIO2PIN & 0x04000000) == 0)
	{
		//printf("\nJoystick-RIGHT pressed!");
		eraseRacket(*currentRacketUpperLeftY, *currentRacketUpperLeftX, *currentRacketLowerRightX, *currentRacketLowerRightY);
		if((*currentRacketLowerRightX + 2) < WIDTH){
			*currentRacketUpperLeftX+=2;
			*currentRacketLowerRightX+=2;
		}
		drawRacket(*currentRacketUpperLeftY, *currentRacketUpperLeftX, *currentRacketLowerRightX, *currentRacketLowerRightY);
		return TRUE;
	}
	if ((FIO2PIN & 0x00800000) == 0)
	{
		//printf("\nJoystick-LEFT pressed!");
		eraseRacket(*currentRacketUpperLeftY, *currentRacketUpperLeftX, *currentRacketLowerRightX, *currentRacketLowerRightY);
		if((*currentRacketUpperLeftX - 2) > DISTANCEFROMTHELEFTEDGE){
			*currentRacketUpperLeftX-=2;
			*currentRacketLowerRightX-=2;
		}
		drawRacket(*currentRacketUpperLeftY, *currentRacketUpperLeftX, *currentRacketLowerRightX, *currentRacketLowerRightY);
		return TRUE;
	}
	return FALSE;
}

void drawMenu(tU16 posX, tU16 posY, char *message){
	lcd_fillScreen(BLACK);
	lcd_putString(posX, posY, message);
	int x,y,z = 0;
	while(1){
		touch_xyz(&x, &y, &z);
		if(0<x && x<240 && 0<y && y<320)
			break;
	}
	lcd_fillScreen(BLACK);
}

void countdown(){
	lcd_fillScreen(BLACK);
	lcd_putString(105, 160, "3");
	delayMs(1000);
	lcd_fillScreen(BLACK);
	lcd_putString(105, 160, "2");
	delayMs(1000);
	lcd_fillScreen(BLACK);
	lcd_putString(105, 160, "1");
	delayMs(1000);
	lcd_fillScreen(BLACK);
}

void rtcInit(){
	//rtc init
    RTC_CCR  = 0x00000012;
    RTC_CCR  = 0x00000010;
    RTC_ILR  = 0x00000007;
    RTC_CIIR = 0x00000000;
    //RTC_AMR  = 0xfe; //0x00000000;
    //RTC_ALSEC = 45;

    delayMs(100);

    RTC_SEC  = 0;
    RTC_MIN  = 0;
    RTC_HOUR = 0;

    delayMs(100);

    RTC_CCR  = 0x00000011;

}

/*****************************************************************************
 *
 * Description:
 *    The main-function. 
 *
 * Returns:
 *    Always 0, since return value is not used.
 *
 ****************************************************************************/
 
int main(void)
{
	//led init
	  IODIR0  |= 0x000F8000;
	  IODIR1  |= 0xFFF00000;
	  FIO2DIR |= 0x0000FFFF;

	  //initialize printf()-functionality
	  eaInit();
	  i2cInit();

	  //initialize PCA9532
	  tU8 initCommand[] = {0x12, 0x97, 0x80, 0x00, 0x40, 0x00, 0x00, 0x00, 0x00};
	  pca9532(initCommand, sizeof(initCommand), NULL, 0);

	//initialize joystick
	IODIR1 |= 0x38000000;
	IOCLR1  = 0x38000000;
	PINSEL3 = 0;

	//Sound
	PINSEL1 &= ~0x00300000;
	PINSEL1 |=  0x00200000;

	pData = (short*)&wavSound[0];
	size = wavSoundSize();

	// initialize external SDRAM
	sdramInit();

  	//initialize LCD  
	lcdInit(&ea_QVGA_v2); 
	lcdTurnOn();

	//initialize touchscreen
	touch_init();
	calibrateStart();

	rtcInit();
  
	drawMenu(100, 160, "PLAY");
	countdown();

	tU16 score = 0;
	tU16 win = 0;
	tU16 gameOver = 0;

	tU16 currentRacketUpperLeftY = DISTANCEFROMTHEBOTTOMEDGE + LENGTH_OF_RACKET_Y;
	tU16 currentRacketUpperLeftX = WIDTH/2 - LENGTH_OF_RACKET_X/2;
	tU16 currentRacketLowerRightX = WIDTH/2 + LENGTH_OF_RACKET_X/2;
	tU16 currentRacketLowerRightY = DISTANCEFROMTHEBOTTOMEDGE;

	tU16 currentCenterX = WIDTH/2;
	tU16 currentCenterY = DISTANCEFROMTHEBOTTOMEDGE + LENGTH_OF_RACKET_Y + BALL_RADIUS;
	tU16 radius = BALL_RADIUS;

	tS16 direction_x = 1;
	tS16 direction_y = -1;

	tU16 flags[3][3] = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};

	for(int j=0; j<3; ++j){
		for(int i=0; i<3; ++i){
			tU16 currentBlockUpperLeftY = HEIGHT - DISTANCEFROMTHEUPEDGE * (i + 1) - i*LENGTH_OF_BLOCK_Y;
			tU16 currentBlockUpperLeftX = DISTANCEFROMTHELEFTEDGE  + (j+1)*DISTANCEFROMTHELEFTEDGE + j*LENGTH_OF_BLOCK_X;
			tU16 currentBlockLowerRightX = DISTANCEFROMTHELEFTEDGE  + (j+1)*DISTANCEFROMTHELEFTEDGE + (j+1)*LENGTH_OF_BLOCK_X;
			tU16 currentBlockLowerRightY = HEIGHT - DISTANCEFROMTHEUPEDGE * (i+1) - (i+1)*LENGTH_OF_BLOCK_Y;

			drawBlock(currentBlockUpperLeftY, currentBlockUpperLeftX, currentBlockLowerRightX, currentBlockLowerRightY, flags[j][i]);
		}
	}
	drawBall(currentCenterX, currentCenterY, radius);
	drawRacket(currentRacketUpperLeftY, currentRacketUpperLeftX, currentRacketLowerRightX, currentRacketLowerRightY);

	//Game *game = newGame(240, 320, 10);
	while(1){
		//lcd_fillScreen(BLACK);
		for(int j=0; j<3; ++j){
			for(int i=0; i<3; ++i){
				tU16 currentBlockUpperLeftY = HEIGHT - DISTANCEFROMTHEUPEDGE * (i + 1) - i*LENGTH_OF_BLOCK_Y;
				tU16 currentBlockUpperLeftX = DISTANCEFROMTHELEFTEDGE  + (j+1)*DISTANCEFROMTHELEFTEDGE + j*LENGTH_OF_BLOCK_X;
				tU16 currentBlockLowerRightX = DISTANCEFROMTHELEFTEDGE  + (j+1)*DISTANCEFROMTHELEFTEDGE + (j+1)*LENGTH_OF_BLOCK_X;
				tU16 currentBlockLowerRightY = HEIGHT - DISTANCEFROMTHEUPEDGE * (i+1) - (i+1)*LENGTH_OF_BLOCK_Y;
				//testCollisionBlockBall(currentBlockUpperLeftY, currentBlockUpperLeftX, currentBlockLowerRightX, currentBlockLowerRightY, flags[j][i], currentCenterX, currentCenterY, radius, &direction_x, &direction_y);
				if(testCollisionBlockBall(currentBlockUpperLeftY, currentBlockUpperLeftX, currentBlockLowerRightX, currentBlockLowerRightY, flags[j][i], currentCenterX, currentCenterY, radius, &direction_x, &direction_y)){
					flags[j][i] = 1;
					score++;
					ledScores(score);
				}
				drawBlock(currentBlockUpperLeftY, currentBlockUpperLeftX, currentBlockLowerRightX, currentBlockLowerRightY, flags[j][i]);
			}
		}
		testCollisionEdgeBall(currentCenterX, currentCenterY, radius, &direction_x, &direction_y, &gameOver);
       
        // To check the racket
		testCollisionBlockBall(currentRacketUpperLeftY, currentRacketUpperLeftX, currentRacketLowerRightX, currentRacketLowerRightY, 0, currentCenterX, currentCenterY, radius, &direction_x, &direction_y);

		eraseBall(currentCenterX, currentCenterY, radius);
		moveBall(&currentCenterX, direction_x, &currentCenterY, direction_y, &radius);
		drawBall(currentCenterX, currentCenterY, radius);

		joystickMovement(&currentRacketUpperLeftY, &currentRacketUpperLeftX, &currentRacketLowerRightX, &currentRacketLowerRightY);

		switch(score){
		case 9:
			win = 1;
			break;
		}
		if(win){
			drawMenu(99, 160, "Win!");
			break;
		}
		if(gameOver){
			drawMenu(97, 160, "Game Over!");
			break;
		}
	}
	return 0;
}
